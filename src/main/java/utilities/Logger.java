package utilities;

import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.RollingFileAppender;

import java.io.IOException;

public class Logger {

    public static final String LOG_TAG = "ROUTE_CALCULATOR: ";
    public static final String ERROR_LOG_PREFIX = "LOG_ERR: ";
    private static final String LOG_FILENAME = "app_logs/route_calculator.log";
    private static final int MAX_BACKUP_INDEX = 3;
    private static final String MAX_FILE_SIZE = "750KB";
    private static final org.apache.log4j.Logger logger = org.apache.log4j.Logger.getLogger(Logger.class);

    public static void initLogger() {
        String fileName = System.getProperty("user.dir") + "/" + LOG_FILENAME;
        logger.setLevel(Level.DEBUG);
        org.apache.log4j.Layout layout = new PatternLayout("%d [%t] %-5p %m%n");
        try {
            logger.removeAllAppenders();
        } catch (Exception e) {
            Logger.e("Unable to remove logger uppenders.");
        }
        try {
            RollingFileAppender rollingFileAppender = new RollingFileAppender(layout, fileName);
            rollingFileAppender.setMaxFileSize(MAX_FILE_SIZE);
            rollingFileAppender.setMaxBackupIndex(MAX_BACKUP_INDEX);
            logger.addAppender(rollingFileAppender);
        } catch (IOException ioe) {
            Logger.e("unable to create log file: " + fileName);
        }
    }

    public static void e(String logMsg) {
        e(logMsg, (Throwable) null);
    }

    public static void w(String logMsg) {
        w(logMsg, (Throwable) null);
    }

    public static void i(String logMsg) {
        i(logMsg, (Throwable) null);
    }

    public static void d(String logMsg) {
        d(logMsg, (Throwable) null);
    }

    public static void e(String logPrefix, String logMsg) {
        e(logPrefix + logMsg);
    }

    public static void w(String logPrefix, String logMsg) {
        w(logPrefix + logMsg);
    }

    public static void i(String logPrefix, String logMsg) {
        i(logPrefix + logMsg);
    }

    public static void d(String logPrefix, String logMsg) {
        d(logPrefix + logMsg);
    }

    public static void e(String logMsg, Throwable t) {
        logMsg = ERROR_LOG_PREFIX + logMsg;
        if (t != null) {
            logger.error(LOG_TAG + logMsg, t);
        } else {
            logger.error(LOG_TAG + logMsg);
            System.out.println("[E] " + logMsg);
        }
    }

    public static void w(String logMsg, Throwable t) {
        if (t != null) {
            logger.warn(LOG_TAG + logMsg, t);
        } else {
            logger.warn(LOG_TAG + logMsg);
            System.out.println("[W] " + logMsg);
        }
    }

    public static void i(String logMsg, Throwable t) {
        if (t != null) {
            logger.info(LOG_TAG + logMsg, t);
        } else {
            logger.info(LOG_TAG + logMsg);
            System.out.println("[I] " + logMsg);
        }
    }

    public static void d(String logMsg, Throwable t) {
        if (t != null) {
            logger.debug(LOG_TAG + logMsg, t);
        } else {
            logger.debug(LOG_TAG + logMsg);
            System.out.println("[D] " + logMsg);
        }
    }
}