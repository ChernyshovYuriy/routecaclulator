package business;

import com.vividsolutions.jts.geom.Coordinate;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/25/13
 * Time: 5:39 PM
 */
public interface IPointSelected {
    void onMousePointSelected(Point screenPosition, Coordinate coordinate);
}