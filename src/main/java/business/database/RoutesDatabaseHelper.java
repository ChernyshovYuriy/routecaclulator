package business.database;

import utilities.Logger;

import java.io.File;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/6/13
 * Time: 4:38 PM
 */
public class RoutesDatabaseHelper extends AppDatabaseBase {

    public static final String ROUTES_TABLE = "ROUTES_TABLE";

    @Override
    public boolean loadDatabase(String databaseFileName, String password, boolean inMemory) {

        boolean success = true;
        File databaseFile = new File(databaseFileName);

        Connection connection;

        try {
            Logger.i("Is Database a Dir: " + databaseFile.isDirectory());
            if (databaseFile.isDirectory()) {
                connection = DriverManager.getConnection(getDatabaseURL(databaseFileName, password, inMemory).get(0));
            } else {
                connection = DriverManager.getConnection(getDatabaseURL(databaseFileName, password, inMemory).get(1));
                try (Statement statement = connection.createStatement()) {
                    statement.executeUpdate("create table " + ROUTES_TABLE + "(" +
                            "id VARCHAR(256), " +
                            "displayName VARCHAR(256), " +
                            "startPointX FLOAT, " +
                            "startPointY FLOAT, " +
                            "destinationPoints BLOB)");
                    statement.close();
                } catch (Throwable throwable) {
                    Logger.e("Load DB Throwable: " + throwable);
                }
            }

            //lookupAllIds = connection.prepareStatement("select id from " + ROUTES_TABLE + " order by id");
            //lookupCredential = connection.prepareStatement("select * from " + ROUTES_TABLE + " where id=?");
            //deleteCredential = connection.prepareStatement("delete from " + ROUTES_TABLE + " where id=?");
            //insertCredential = connection.prepareStatement("insert into " + ROUTES_TABLE + " values (?,?,?,?,?)");
            //searchCredential = connection.prepareStatement("select * from " + ROUTES_TABLE + " " +
            //        "where id like ? and displayName like ?");
        } catch (SQLException e) {
            success = false;
            Logger.e("Load DB SQLException: " + e);
            /*if (causedBy(e, "XBM06")) {
                JOptionPane.showMessageDialog(this,
                        "Wrong master password",
                        "MyError",
                        JOptionPane.ERROR_MESSAGE);
            } else {
                String err = collectErrorString(e);
                JOptionPane.showMessageDialog(this,
                        "Could not create or boot credentials database: \n" +
                                err,
                        "MyError",
                        JOptionPane.ERROR_MESSAGE);
            }*/
        }

        return success;
    }

    /*private boolean causedBy(SQLException e, String s) {
        boolean result = false;
        for (SQLException exception = e; exception != null; exception = exception.getNextException()) {
            if (s.equals(exception.getSQLState())) {
                result = true;
                break;
            }
        }
        return result;
    }*/
}