package business.database;

import java.sql.ResultSet;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 11:14 AM
 */
public interface IDatabaseBase {
    boolean loadDatabase(String databaseFileName, String password, boolean inMemory);
    boolean insertRecord(DatabaseRowEntity entity);
    ResultSet readRecord(DatabaseRowEntity entity);
}