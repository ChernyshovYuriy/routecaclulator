package business.database;

import utilities.Logger;

import java.io.File;
import java.sql.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 11:11 AM
 */
public class AppSettingsDatabaseHelper extends AppDatabaseBase {

    public static final String APPLICATION_SETTINGS_TABLE = "APPLICATION_SETTINGS_TABLE";

    @Override
    public boolean loadDatabase(String databaseFileName, String password, boolean inMemory) {

        boolean success = Boolean.TRUE;
        File databaseFile = new File(databaseFileName);

        try {
            Logger.i("Is Database a Dir: " + databaseFile.isDirectory());
            if (databaseFile.isDirectory()) {
                connection = DriverManager.getConnection(getDatabaseURL(databaseFileName, password, inMemory).get(0));
            } else {
                connection = DriverManager.getConnection(getDatabaseURL(databaseFileName, password, inMemory).get(1));
                try (Statement statement = connection.createStatement()) {
                    statement.executeUpdate("create table " + APPLICATION_SETTINGS_TABLE + "(" +
                            "id VARCHAR(256), " +
                            "lastUsedSHPurl VARCHAR(256))");
                    statement.close();
                } catch (Throwable throwable) {
                    Logger.e("Load DB Throwable: " + throwable);
                }
            }

            //lookupAllIds = connection.prepareStatement("select id from " + APPLICATION_SETTINGS_TABLE + " order by id");
            //lookupCredential = connection.prepareStatement("select * from " + APPLICATION_SETTINGS_TABLE + " where id=?");
            //deleteCredential = connection.prepareStatement("delete from " + APPLICATION_SETTINGS_TABLE + " where id=?");
            //insertCredential = connection.prepareStatement("insert into " + APPLICATION_SETTINGS_TABLE + " values (?,?)");
            //searchCredential = connection.prepareStatement("select * from " + APPLICATION_SETTINGS_TABLE + " " +
            //        "where id like ?");
        } catch (SQLException e) {
            success = Boolean.FALSE;
            Logger.e("Load DB SQLException: " + e);
        }

        return success;
    }

    @Override
    public boolean insertRecord(DatabaseRowEntity entity) {
        if (connection == null) {
            Logger.w(getClass().getSimpleName() + " connection is null");
            return Boolean.FALSE;
        }

        switch (entity.getInsertCase()) {
            case LAST_PATH_TO_SHP:
                return insertLastUsedSHPurl(entity);
            default:
                return Boolean.FALSE;
        }
    }

    @Override
    public ResultSet readRecord(DatabaseRowEntity entity) {
        switch (entity.getSelectCase()) {
            case LAST_PATH_TO_SHP:
                return selectLastUsedSHPurl(entity);
            default:
                return null;
        }
    }

    private ResultSet selectLastUsedSHPurl(DatabaseRowEntity entity) {
        if (connection == null) {
            Logger.w(getClass().getSimpleName() + " connection is null");
            return null;
        }

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from " + APPLICATION_SETTINGS_TABLE + " " +
                    "where id like ?");
            preparedStatement.setString(1, entity.getId());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (!resultSet.next()) {
             return null;
            }
            return resultSet;
        } catch (SQLException e) {
            Logger.e(getClass().getSimpleName() + " select last used SHP file error " + e);
        }
        return null;
    }

    private boolean insertLastUsedSHPurl(DatabaseRowEntity entity) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement("insert into " + APPLICATION_SETTINGS_TABLE + " values (?,?)");
            preparedStatement.setString(1, entity.getId());
            preparedStatement.setString(2, entity.getLastUsedSHPurl());
            int result = preparedStatement.executeUpdate();
            Logger.i(getClass().getSimpleName() + " Insert last used SHP url: " + result);
        } catch (SQLException e) {
            /*String err = collectErrorString(e);
            JOptionPane.showMessageDialog(this,
                    "Error inserting credential: \n" +
                            err,
                    "MyError",
                    JOptionPane.ERROR_MESSAGE);*/
            Logger.e(getClass().getSimpleName() + " Insert last used SHP url error " + e);
            return Boolean.FALSE;
        }
        return Boolean.TRUE;
    }
}