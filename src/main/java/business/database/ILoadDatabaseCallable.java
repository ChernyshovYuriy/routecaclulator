package business.database;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 7:21 PM
 */
public interface ILoadDatabaseCallable {
    void onDatabaseLoad();
}