package business.database;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 11:34 AM
 */
public class DatabaseRowEntity {

    public enum INSERT_CASE {LAST_PATH_TO_SHP};
    public enum SELECT_CASE {LAST_PATH_TO_SHP};

    private String mLastUsedSHPurl;
    private String mId;
    private INSERT_CASE mInsertCase;
    private SELECT_CASE mSelectCase;

    public String getLastUsedSHPurl() {
        return mLastUsedSHPurl;
    }

    public void setLastUsedSHPurl(String mLastUsedSHPurl) {
        this.mLastUsedSHPurl = mLastUsedSHPurl;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public INSERT_CASE getInsertCase() {
        return mInsertCase;
    }

    public void setInsertCase(INSERT_CASE mInsertCase) {
        this.mInsertCase = mInsertCase;
    }

    public SELECT_CASE getSelectCase() {
        return mSelectCase;
    }

    public void setSelectCase(SELECT_CASE mSelectCase) {
        this.mSelectCase = mSelectCase;
    }
}