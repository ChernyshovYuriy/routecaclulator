package business.database;

import utilities.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 11:08 AM
 */
public class AppDatabaseBase implements IDatabaseBase {

    public static final String APPLICATION_SETTINGS_TABLE_ID = "mIrthDhaKs13";
    public static final String APPLICATION_DATABASE = "APPLICATION_DATABASE";
    public static final String APPLICATION_DATABASE_PASSWORD = "nEistHat60";

    protected Connection connection;

    public boolean loadDatabase(String databaseFileName, String password, boolean inMemory) {
        return Boolean.FALSE;
    }

    public boolean insertRecord(DatabaseRowEntity entity) {
        return Boolean.FALSE;
    }

    public ResultSet readRecord(DatabaseRowEntity entity) {
        return null;
    }

    protected ArrayList<String> getDatabaseURL(String databaseFileName, String password, boolean inMemory) {
        String existsURL;
        String newURL;
        if (inMemory) {
            Logger.i("Database in-memory: " + databaseFileName);
            existsURL = "jdbc:derby:memory:" + // restore existing backup
                    databaseFileName + ";restoreFrom=" + databaseFileName +
                    ";bootPassword=" + password;
            newURL = "jdbc:derby:memory:" + databaseFileName +
                    ";create=true;dataEncryption=true;bootPassword=" + password;
        } else {
            Logger.i("Database on-disk: " + databaseFileName);
            existsURL = "jdbc:derby:" + databaseFileName +
                    ";bootPassword=" + password;
            newURL = "jdbc:derby:" + databaseFileName +
                    ";create=true;dataEncryption=true;bootPassword=" + password;
        }
        Logger.i("Database exists url: " + existsURL);
        Logger.i("Database new    url: " + newURL);
        ArrayList<String> result = new ArrayList<>(2);
        result.add(existsURL);
        result.add(newURL);
        return result;
    }
}