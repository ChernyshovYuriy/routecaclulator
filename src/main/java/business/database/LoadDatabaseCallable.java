package business.database;

import java.util.concurrent.Callable;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/7/13
 * Time: 7:18 PM
 */
public class LoadDatabaseCallable implements Callable<Boolean> {

    private AppDatabaseBase mDatabaseHelper;
    private ILoadDatabaseCallable mDatabaseCallable;

    public LoadDatabaseCallable(AppDatabaseBase databaseHelper, ILoadDatabaseCallable databaseCallable) {
        mDatabaseHelper = databaseHelper;
        mDatabaseCallable = databaseCallable;
    }

    @Override
    public Boolean call() throws Exception {
        boolean result = mDatabaseHelper.loadDatabase(AppSettingsDatabaseHelper.APPLICATION_SETTINGS_TABLE,
                RoutesDatabaseHelper.APPLICATION_DATABASE_PASSWORD, Boolean.FALSE);
        mDatabaseCallable.onDatabaseLoad();
        return result;
    }
}