package business;

import com.vividsolutions.jts.geom.Coordinate;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/26/13
 * Time: 8:19 AM
 */
public interface IPointPopUpMenuSelected {
    void onStartPointItemSelected(Point screenPosition, Coordinate pointCoordinate);
    void onPointItemSelected(Point screenPosition, Coordinate pointCoordinate);
}