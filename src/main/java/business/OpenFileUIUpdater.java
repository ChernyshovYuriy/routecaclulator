package business;

import javax.swing.*;
import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/24/13
 * Time: 11:41 PM
 */
public class OpenFileUIUpdater implements IOpenFile {

    JTextField mTextField;

    public OpenFileUIUpdater(JTextField textField) {
     mTextField = textField;
    }

    @Override
    public void onFileOpen(File file) {
        if (mTextField != null && file != null) {
            mTextField.setText(file.getName());
        }
    }
}