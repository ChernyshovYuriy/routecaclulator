package business;

import business.database.*;
import org.geotools.data.FileDataStore;
import org.geotools.data.FileDataStoreFinder;
import org.geotools.data.simple.SimpleFeatureSource;
import org.geotools.swing.data.JFileDataStoreChooser;
import utilities.Logger;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.concurrent.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/24/13
 * Time: 5:19 PM
 */
public class ShapeFileProcessor implements ILoadDatabaseCallable {

    public void getShapeFile(IOpenFile openFileCallback) {
        final AppDatabaseBase databaseHelper = new AppSettingsDatabaseHelper();

        Callable<Boolean> callable = new LoadDatabaseCallable(databaseHelper, this);
        ExecutorService pool = Executors.newSingleThreadExecutor();
        Future<Boolean> future = pool.submit(callable);
        /*try {
            Logger.i("TRACE 0 " + Thread.currentThread().getName());
            isDatabaseOpenSuccess = future.get();
        } catch (InterruptedException e) {
            Logger.e(getClass().getSimpleName() + " Future get error " + e);
        } catch (ExecutionException e) {
            Logger.e(getClass().getSimpleName() + " Future get error " + e);
        }*/

        String lastUsedSHPurl = null;
        boolean isDatabaseOpenSuccess = Boolean.FALSE;
        if (isDatabaseOpenSuccess) {
            DatabaseRowEntity selectEntity = new DatabaseRowEntity();
            selectEntity.setId(AppDatabaseBase.APPLICATION_SETTINGS_TABLE_ID);
            selectEntity.setSelectCase(DatabaseRowEntity.SELECT_CASE.LAST_PATH_TO_SHP);
            ResultSet resultSet = databaseHelper.readRecord(selectEntity);
            if (resultSet != null) {
                try {
                    lastUsedSHPurl = resultSet.getString(2);
                    Logger.d(getClass().getSimpleName() + " saved last SHP path " + lastUsedSHPurl);
                } catch (SQLException e) {
                    Logger.e(getClass().getSimpleName() + " get last used SHP path error " + e);
                } finally {
                    try {
                        resultSet.close();
                    } catch (SQLException e) {
                        Logger.e(getClass().getSimpleName() + " resultSet.close() error " + e);
                    }
                }
            }
        }

        Logger.d(getClass().getSimpleName() + " Start to open SHP file");
        File file = JFileDataStoreChooser.showOpenFile("shp", lastUsedSHPurl != null ? new File(lastUsedSHPurl) : null, null);
        Logger.d(getClass().getSimpleName() + " Open shape file " + file);

        openFileCallback.onFileOpen(file);

        /*if (isDatabaseOpenSuccess) {
            Logger.i(getClass().getSimpleName() + " last path to SHP file " + file.getParent());
            DatabaseRowEntity insertEntity = new DatabaseRowEntity();
            insertEntity.setInsertCase(DatabaseRowEntity.INSERT_CASE.LAST_PATH_TO_SHP);
            insertEntity.setLastUsedSHPurl(file.getParent());
            insertEntity.setId(AppDatabaseBase.APPLICATION_SETTINGS_TABLE_ID);
            databaseHelper.insertRecord(insertEntity);
        }

        Logger.i(getClass().getSimpleName() + " App Settings DB open " + isDatabaseOpenSuccess);
        */
    }

    public SimpleFeatureSource getSimpleFeatureSource(File file) {
        FileDataStore store = null;
        try {
            store = FileDataStoreFinder.getDataStore(file);
        } catch (IOException e1) {
            Logger.e(getClass().getSimpleName() + " FileDataStoreFinder.getDataStore", e1);
        }
        SimpleFeatureSource featureSource = null;
        try {
            if (store != null) {
                featureSource = store.getFeatureSource();
            }
        } catch (IOException e1) {
            Logger.e(getClass().getSimpleName() + " store.getFeatureSource", e1);
        }
        return featureSource;
    }

    public void onDatabaseLoad() {

    }
}