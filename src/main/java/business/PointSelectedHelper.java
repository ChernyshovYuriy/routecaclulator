package business;

import com.vividsolutions.jts.geom.Coordinate;
import org.geotools.swing.event.MapMouseEvent;

import java.awt.*;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/25/13
 * Time: 5:48 PM
 */
public class PointSelectedHelper implements IPointSelected {

    Coordinate mCoordinate;
    Point mPoint;

    public Coordinate getCoordinate() {
        return mCoordinate;
    }

    public Point getMapMouseEvent() {
        return mPoint;
    }

    public void onMousePointSelected(Point screenPosition, Coordinate coordinate) {
        mCoordinate = coordinate;
        mPoint = screenPosition;
    }
}