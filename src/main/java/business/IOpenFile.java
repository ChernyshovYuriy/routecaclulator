package business;

import java.io.File;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/24/13
 * Time: 11:37 PM
 */
public interface IOpenFile {
    void onFileOpen(File file);
}