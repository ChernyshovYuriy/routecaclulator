package business;

import com.vividsolutions.jts.geom.Coordinate;
import org.geotools.geometry.DirectPosition2D;
import org.geotools.swing.event.MapMouseAdapter;
import org.geotools.swing.event.MapMouseEvent;
import utilities.Logger;

import javax.swing.*;
import java.awt.event.MouseEvent;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/25/13
 * Time: 12:01 AM
 */
public class MapMouseListener extends MapMouseAdapter {

    JPopupMenu mPopup;
    IPointSelected mPointSelectedCallback;

    public MapMouseListener(JPopupMenu mPopup, IPointSelected pointSelectedCallback) {
        this.mPopup = mPopup;
        mPointSelectedCallback = pointSelectedCallback;
    }

    public void onMouseReleased(MapMouseEvent mapMouseEvent) {
        showPopup(mapMouseEvent);
    }

    public void onMousePressed(MapMouseEvent mapMouseEvent) {
        // print the screen and world position of the mouse
        showPopup(mapMouseEvent);

        Logger.i("Mouse position at");
        Logger.i("  screen: " + mapMouseEvent.getX() + " " + mapMouseEvent.getY());

        DirectPosition2D pos = mapMouseEvent.getWorldPos();
        Logger.i("  world: " + pos.x + " " + pos.y);
        mPointSelectedCallback.onMousePointSelected(mapMouseEvent.getPoint(), new Coordinate(pos.x, pos.y));
    }

    public void onMouseEntered(MapMouseEvent ev) {
        //Logger.i("Mouse entered map pane");
    }

    public void onMouseExited(MapMouseEvent ev) {
        //Logger.i("Mouse left map pane");
    }

    private void showPopup(MouseEvent mouseEvent) {
        if (mPopup != null && mouseEvent.isPopupTrigger()) {
            mPopup.show(mouseEvent.getComponent(), mouseEvent.getX(), mouseEvent.getY());
        }
    }
}