import utilities.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/24/13
 * Time: 4:01 PM
 */
public class Main {

    public Main() {
        Logger.i("Application constructor");
        new MainView();
    }

    public static void main(String[] args) throws Exception {
        Thread.currentThread().setName("MainThread");
        Logger.initLogger();
        Logger.i("Init application");
        new Main();
    }
}