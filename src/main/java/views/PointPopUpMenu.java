package views;

import business.IPointPopUpMenuSelected;
import business.PointSelectedHelper;
import com.vividsolutions.jts.geom.Point;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 3/26/13
 * Time: 8:08 AM
 */
public class PointPopUpMenu {

    private JPopupMenu popupMenu;

    public PointPopUpMenu(final PointSelectedHelper pointSelectedHelper,
                          final IPointPopUpMenuSelected popUpMenuSelectedCallback) {
        popupMenu = new JPopupMenu();
        final JMenuItem menuItem_StartPoint = new JMenuItem("Set Start Point here");
        menuItem_StartPoint.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popUpMenuSelectedCallback.onStartPointItemSelected(pointSelectedHelper.getMapMouseEvent(), pointSelectedHelper.getCoordinate());
                popupMenu.remove(menuItem_StartPoint);
            }
        });
        popupMenu.add(menuItem_StartPoint);
        JMenuItem menuItem = new JMenuItem("Set Point here");
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                popUpMenuSelectedCallback.onPointItemSelected(pointSelectedHelper.getMapMouseEvent(), pointSelectedHelper.getCoordinate());
            }
        });
        popupMenu.add(menuItem);
    }

    public JPopupMenu getPopupMenu() {
        return popupMenu;
    }
}