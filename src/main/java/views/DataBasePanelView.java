package views;

import business.database.AppDatabaseBase;
import business.database.RoutesDatabaseHelper;
import utilities.Logger;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created with IntelliJ IDEA.
 * User: chernyshovyuriy
 * Date: 4/3/13
 * Time: 10:04 PM
 */
public class DataBasePanelView extends JFrame {

    private JPanel dataBaseViewPanel;
    private JList savedRoutesList;
    private JButton removeRouteButton;
    private JButton loadRouteBtn;
    private JButton saveRouteButton;
    DefaultListModel<String> listModel;

    public DataBasePanelView() throws HeadlessException {
        Logger.i(getClass().getSimpleName());

        setTitle("DataBase Panel");
        setContentPane(dataBaseViewPanel);
        setSize(400, 200);
        setVisible(true);

        setLocationRelativeTo(null);

        listModel = new DefaultListModel<String>();

        savedRoutesList.setModel(listModel);
        savedRoutesList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        savedRoutesList.setLayoutOrientation(JList.VERTICAL_WRAP);
        savedRoutesList.setVisibleRowCount(-1);

        if (listModel.isEmpty()) {
            loadRouteBtn.setEnabled(Boolean.FALSE);
            removeRouteButton.setEnabled(Boolean.FALSE);
        }

        saveRouteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                AppDatabaseBase databaseHelper = new RoutesDatabaseHelper();
                boolean loadSuccess = databaseHelper.loadDatabase(RoutesDatabaseHelper.APPLICATION_DATABASE, RoutesDatabaseHelper.APPLICATION_DATABASE_PASSWORD, false);
                Logger.d("DB Load success " + loadSuccess);
            }
        });
    }
}